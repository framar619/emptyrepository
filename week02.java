import javax.xml.namespace.QName;
import java.util.*;

public class week02 {

    public static void main(String[] args) {

        System.out.println("-- List --");
        List list = new ArrayList();
        list.add("My");
        list.add("pet");
        list.add("parrot");
        list.add("is");
        list.add("Polly");
        list.add("My");

        for (Object str : list) {
            System.out.println((String)str);
        }

        System.out.println("-- Set --");
        Set set = new TreeSet();
        set.add("My");
        set.add("pet");
        set.add("parrot");
        set.add("is");
        set.add("Polly");
        set.add("Mi");

        for (Object str : set) {
            System.out.println((String)str);
        }

        System.out.println("-- Queue --");
        Queue queue = new PriorityQueue();
        queue.add("My");
        queue.add("pet");
        queue.add("parrot");
        queue.add("is");
        queue.add("Polly");
        queue.add("My");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("-- Map --");
        Map map = new HashMap();
        map.put(1,"My");
        map.put(2,"pet");
        map.put(3,"parrot");
        map.put(4,"is");
        map.put(5,"Polly");
        map.put(4,"My");

        for (int i = 1; i < 6; i++) {
            String result = (String)map.get(i);
            System.out.println(result);
        }

        for (Object str : list) {
            System.out.println((String)str);
        }

        System.out.println("-- List of my favorite Pets --");
        List<pets> myList = new LinkedList<pets>();
        myList.add(new pets("Polly   ", "Parrot"));
        myList.add(new pets("Mailo   ", "Dog"));
        myList.add(new pets("Garfield", "Cat"));
        myList.add(new pets("Alvin   ", "Squirrel"));

        for (pets pet : myList)
            System.out.println(pet);

    }

}
